/**
*  LIC - ISEL 2021/22
*
*  App - controla todo o mecanismo software da vending machine
*        é responsável por iniciar todos os blocos de hardware bem como o loop
*        infinito (run) do funcionamento da máquina
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
*
*/
package main.kotlin

import isel.leic.utils.Time
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object App {
    //<editor-fold desc="CONSTANTS & VARIABLES">
    private const val KEY_CANCEL = '#'
    private const val COIN_VALUE = 50
    private const val MAX_PRODUCT_TYPE = 20
    private const val CONFIRMATION_TIMEOUT = 5000L
    private const val MAINTENANCE_MENUTIME = 2000L
    private const val SELECTION_TIME = 10000L
    private const val TIME_LINE = 1250L
    private lateinit var timeStamp: LocalDateTime
    private val TIME_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")
    private var menuIdx = 0
    private val menuKey = charArrayOf('1', '2', '3', '4')
    private val menuText = arrayOf("Dispense test", "Update Prod.", "Remove Prod.", "Shutdown")
    private var maintenanceTimeStamp = 0L
    //</editor-fold>

    //<editor-fold desc="INIT, RUN & CLOSE">
    fun init(){
        TUI.init()
        CoinAcceptor.init()
        Dispenser.init()
        M.init()
        Products.load()
        CoinDeposit.load()
    }

    fun run(){
        var shutdown = false
        update(true)

        while (!shutdown) {
            if (M.isMaintenanceMode()) {
                val maintenance = maintenance()
                shutdown = maintenance
                if (!maintenance) {
                    update(true)
                    continue
                }
            }
            else {
                showTime(false)
                if (TUI.getKey() == '#') {
                    vending(true)
                    update(false)
                }
            }
        }
        return
    }

    fun close(){
        TUI.clear()
        TUI.writeString(0, TUI.Align.CENTER, "Shutdowning ...", false)
        Products.save()
        CoinDeposit.save()
        TUI.off()
        return
    }
    //</editor-fold>

    //<editor-fold desc="IDLE">
    private fun update(force: Boolean){
        TUI.clear()
        TUI.writeString(0, TUI.Align.CENTER, "Vending Machine", false)
        showTime(true)
    }

    private fun showTime(force: Boolean){
        if (!::timeStamp.isInitialized) {
            timeStamp = LocalDateTime.now()
            TUI.writeString(1, TUI.Align.CENTER, timeStamp.format(TIME_FORMAT), false)
        }
        else if (::timeStamp.isInitialized && (timeStamp.minute != LocalDateTime.now().minute) || force ) {
            timeStamp = LocalDateTime.now()
            TUI.writeString(1, TUI.Align.CENTER, timeStamp.format(TIME_FORMAT), true)
        }
    }
    //</editor-fold>

    //<editor-fold desc="VENDING">
    private fun vending(sellingMode: Boolean){
        val products = Products.getProducts()
        val p = TUI.selectItem(products, SELECTION_TIME, true)
        if (p != null) {
            var credit = 0
            if (sellingMode) { TUI.writeString(1, TUI.Align.CENTER, TUI.moneyFormat(p.getPrice()), true)}
            else { TUI.writeString(1, TUI.Align.CENTER, "*- to Print", true) }

            while (credit < p.getPrice()){
                if (sellingMode) {
                    val key = TUI.getKey()
                    if (CoinAcceptor.hasCoin()) {
                        CoinAcceptor.acceptCoin()
                        credit += COIN_VALUE
                        TUI.writeString(1, TUI.Align.CENTER, TUI.moneyFormat(p.getPrice() - credit), true)
                    }
                    if (key == KEY_CANCEL) {
                        if (credit != 0) {
                            TUI.message("Vending Aborted ...", "Return " + TUI.moneyFormat(credit), TIME_LINE)
                            CoinDeposit.update(-credit)
                            CoinAcceptor.ejectCoins()
                        } else {
                            TUI.message("Vending Aborted ...", "", TIME_LINE)
                        }
                        return
                    }
                    continue
                }
                break
            }
            TUI.writeString(1, TUI.Align.CENTER, "Collect Product", true)
            if (sellingMode) {
                CoinAcceptor.collectCoins()
                CoinDeposit.update(credit)
                p.updateStock(p.getStock() - 1)
                Dispenser.dispense(p.getID())
                while (!Dispenser.isFinished()) { }
                TUI.clear()
                TUI.message("Thank you", " see you again", TIME_LINE)
                return
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="MAINTENANCE">
    private fun maintenance(): Boolean {
        maintenanceMenu()
        do {
            if (!M.isMaintenanceMode()) return false
            updateMaintenanceInfo()
            val products = Products.getProducts()

            when (TUI.getKey()) {
                '1' -> {
                    vending(false)
                    maintenanceMenu()
                    break
                }
                '2' -> {
                    val p = TUI.selectItem(products, SELECTION_TIME, false)!!
                    TUI.writeString(1, TUI.Align.CENTER, p.getStock().toString(), true)
                    var newQty = 0
                    do {
                        newQty = TUI.getInteger(1, 0, "Qty:", 2, Character.MIN_VALUE, true)
                    } while (newQty > MAX_PRODUCT_TYPE)
                    if (TUI.yesNo("Update " + p.getName() + '=' + newQty, SELECTION_TIME)) {
                        p.updateStock(newQty)
                    }
                    maintenanceMenu()
                    break
                }
                '3' -> {
                    val p = TUI.selectItem(products, SELECTION_TIME, false)!!
                    if (TUI.yesNo("Remove " + p.getName(), SELECTION_TIME)) {
                        Products.remove(p.getID())
                    }
                    maintenanceMenu()
                    break
                }
                '4' -> return TUI.yesNo("Shutdown", CONFIRMATION_TIMEOUT)
            }
        } while (M.isMaintenanceMode())
        return false
    }

    private fun maintenanceMenu(){
        TUI.clear()
        TUI.writeString(0, TUI.Align.CENTER, "Maintenance Mode", false)
        updateMaintenanceInfo()
    }

    private fun updateMaintenanceInfo() {
        if (maintenanceTimeStamp < Time.getTimeInMillis()) {
            maintenanceTimeStamp = Time.getTimeInMillis() + MAINTENANCE_MENUTIME
            TUI.writeString(1, TUI.Align.LEFT, menuKey[menuIdx].toString() + "-" + menuText[menuIdx], true)
            ++menuIdx
            menuIdx = if (menuIdx < menuKey.size) menuIdx else 0
        }
    }
    //</editor-fold>
}