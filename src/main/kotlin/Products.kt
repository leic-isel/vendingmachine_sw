/*
*  LIC - ISEL 2021/22
*
*  CoinDeposit - responsavel por manipular (ler, atualizar e guardar)
*                o ficheiro PRODUCTS.txt
*                transforma ainda cada linha do ficheiro num objeto e
*                controi uma lista desses objetos
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import java.io.FileNotFoundException

object Products {
    private var productsFile =  FileAccess
    private var products: ArrayList<Product> = ArrayList()

    fun init() = productsFile.init("PRODUCTS.txt")

    fun load(): Boolean {
        init()
        return try {
            val fileProducts = readFile()
            if (products.size > 1) products.clear()
            for (line in fileProducts){ products.add(Product(line.split(";"))) }
            true
        } catch(error: Throwable){
            println("Error loading file PRODUCTS.txt")
            false
        }
    }

    fun save(): Boolean {
        return try {
            productsFile.clear()
            var size = products.size

            for (product in products) {
                val p = product.getProduct()
                if (size > 1) { productsFile.write("${p}\n") }
                else  { productsFile.write("$p") }
                size--
            }
            close()
            true
        }
        catch (error: FileNotFoundException) {
            println("Error saving file PRODUCTS.txt")
            false
        }
        finally {close()}
    }

    fun get(id: Int): Product? {
        for (p in products) { if (p.getID() == id) return p }
        return null
    }

    fun getProducts() = products

    fun remove(id: Int){ if (get(id) != null) { products.remove(get(id)) } }

    private fun close() = productsFile.close()

    private fun readFile() = productsFile.read()
}
