/*
*  LIC - ISEL 2021/22
*
*  KBD - responsavel por ler teclas, métodos retornam '0'..'9', '#', '*' ou NONE
*                o ficheiro CoinDeposit.txt
*
*   O signal para converter em carater vem da camada HAL e, com base nos bits
*   do sinal pretendido obtemos o valor lido do teclado
*
*   Para ler do teclado, é enviado para a instancia de HAL a mascara para obter
*   do sinal os indicador pretendido:
*   Dval: indicação de valor em envio
*   D0..3: valor da tecla premida
*
*   Para terminar o envio do sinal do teclado, é necessária comunicar o ACK, para
*   isso também é enviada uma mascara para o bit necessário.
*
*    O sinal recebido na camada HAL tem a dimensão de 1 byte (8 bits), pelo que
*    definimos para as várias máscaras os seguintes bits:
*      - Dval (VAL):  0x10
*      - Data (DATA): 0x0f
*      - ACK (ACK):   0x80
*
*          7    6   5    4      3    2    1   0
*    In    0    0   0  (DVAL) (D3) (D2) (D1) (D0)
*    Out (ACK)  0   0    0      0    0    0   0
*
*
*    Apos receção do valor do teclado, é necessário mapear o valor para o carater
*    do enviado:
*      ____________________
*     |      |      |      |
*     |  1   |  2   |  3   | r0
*     | 0000 | 0100 | 1000 |
*     |--------------------|
*     |      |      |      |
*     |  4   |  5   |  6   | r1
*     | 0001 | 0101 | 1001 |
*     |--------------------|
*     |      |      |      |
*     |  7   |  8   |  9   | r2
*     | 0010 | 0110 | 1010 |
*     |--------------------|
*     |      |      |      |
*     |  *   |  0   |  #   | r3
*     | 0011 | 0111 | 1011 |
*     |______|______|______|
*        c0     c1     c2
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import isel.leic.utils.Time

// ler teclas. Métodos retornam '0'..'9', '#', '*' ou NONE
object KBD {
    private const val NONE = Character.MIN_VALUE
    private val KEYMAP = charArrayOf('1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '0', '#') // to Simulator
    //private val KEYMAP = charArrayOf('*', '7', '4', '1', '0', '8', '5', '2', '#', '9', '6', '3') // to our Hardware
    private const val K_VAL_MASK = 0x10
    private const val K_ACK_MASK = 0x80
    private const val K_DATA_MASK = 0xF

    // inicia a classe
    fun init() = HAL.clrBits(K_VAL_MASK)

    // retorna de imediato a tecla premida ou NONE se não há tecla premida
    fun getKey(): Char {
        var key = NONE

        if (HAL.isBit(K_VAL_MASK)){
            key = KEYMAP[HAL.readBits(K_DATA_MASK)]
            HAL.setBits(K_ACK_MASK)
            while (HAL.isBit(K_VAL_MASK)) Time.sleep(100L)
            HAL.clrBits(K_ACK_MASK)
        }
        return key
    }

    // retorna quando a tecla for premida ou NONE após decorrido 'timeout' milisegundos
    fun waitKey(timeout: Long): Char {
        val finishTime = Time.getTimeInMillis() + timeout
        var key = NONE
        while (Time.getTimeInMillis() < finishTime && key == NONE) {
            val t1 = Time.getTimeInMillis()
            key = getKey()
            if (key != NONE){
                val timeToGetKey = Time.getTimeInMillis() - t1
                println("Tempo a percorrer as teclas: $timeToGetKey miliseconds")
            }
        }
        return key
    }
}
