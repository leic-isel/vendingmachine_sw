/*
*  LIC - ISEL 2021/22
*
*  TUI - introduz os métodos necessários para abstrair a interação com o LCD e o KBD
*        Os métodos permitem responder às necessidades da aplicação para
*        apresentar informação no LCD bem como obter valores ou comandos dados
*        pelo utilizador através do KBD.
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import kotlin.collections.ArrayList

object TUI {
    private const val NONE = Character.MIN_VALUE
    private const val KEY_CHANGE = '*'
    private const val KEY_DOWN = '8'
    private const val KEY_SELECT = '#'
    private const val KEY_UP = '2'
    private const val KEY_YES = '5'
    private const val TIME_LINE = 5000L
    private var signGraph = "\u0004"

    enum class Align{LEFT, CENTER, RIGHT}

    fun init(){
        HAL.init()
        SerialEmitter.init()
        KBD.init()
        LCD.init()
        on()
    }

    fun getKey(): Char = KBD.getKey()

    fun clear() = LCD.clear()

    fun moneyFormat(value: Int): String = "${value*0.01}\u0000"

    fun message(line1: String?, line2: String?, timeout: Long): Char {
        if (line1 != null) { writeString(0, Align.CENTER, line1, true) }
        if (line2 != null) { writeString(1, Align.CENTER, line2, true) }
        return KBD.waitKey(timeout)
    }

    fun yesNo(txt: String, timeout: Long): Boolean {
        writeString(0, Align.CENTER, txt, false)
        writeString(1, Align.CENTER, "5-Yes  other-No", false)
        return KBD.waitKey(timeout) == KEY_YES
    }

    fun writeString(line: Int, align: Align, str: String, clear: Boolean){
        when (align.ordinal) {
            0 -> {
                posCursor(line, 0)
                writeString(str)
                if (clear) { writeNchar(' ', 16 - str.length) }
            }
            1 -> if (clear) {
                posCursor(line, 0)
                writeNchar(' ', (16 - str.length) / 2)
                writeString(str)
                writeNchar(' ', (16 - str.length) / 2 + str.length % 2)
            } else {
                posCursor(line, 8 - str.length / 2 - str.length % 2)
                writeString(str)
            }
            2 -> if (clear) {
                posCursor(line, 0)
                writeNchar(' ', 16 - str.length)
                writeString(str)
            } else {
                posCursor(line, 16 - str.length)
                writeString(str)
            }
        }
    }

    fun getInteger(lin: Int, col: Int, txt: String, digits: Int, show: Char, clrLine: Boolean): Int {
        var value = 0
        LCD.posCursor(lin, col)
        writeString(txt)
        writeNchar('?', digits)
        if (clrLine) { writeNchar(' ', 16 - txt.length) }

        val col = col + txt.length
        LCD.posCursor(lin, col)
        LCD.cursorON()

        var i = 0
        while (i < digits && value >= 0) {
            val c = KBD.waitKey(TIME_LINE)
            if (c.code == 0) { value = 0 }
            else {
                if (c in '0'..'9') {
                    value = value * 10 + c.code - 48
                    LCD.writeChar(if (show.code == 0) c else show)
                    ++i
                    continue
                }
                else { --i }
            }
            ++i
        }
        LCD.cursorOFF()
        return value
    }

    fun off() = LCD.off()

    private fun on() = LCD.on()

    private fun posCursor(line: Int, column: Int) = LCD.posCursor(line, column)

    private fun writeString(txt: String?){ if (txt != null) { LCD.writeString(txt) }}

    private fun printProduct(show: Product){
        clear()
        writeString(0, Align.CENTER, show.getName(), false)
        var str = "%02d$signGraph"
        val productsId = arrayOf(show.getID())
        val idStr = String.format(str, *productsId.copyOf(productsId.size))
        writeString(1, Align.LEFT, idStr, false)
        str = "#%02d"
        val productsStock = arrayOf(show.getStock())
        val stockStr = String.format(str, *productsStock.copyOf(productsStock.size))
        writeString(1, Align.CENTER, stockStr, false)
        writeString(1, Align.RIGHT, moneyFormat(show.getPrice()), false)
    }

    private fun waitKey(timeout: Long): Char = KBD.waitKey(timeout)

    private fun writeNchar(c: Char, num: Int){
        var i = 0
        while (i < num) {
            LCD.writeChar(c)
            ++i
        }
    }

    fun selectItem(selectionItems: ArrayList<Product>, timeout: Long, available: Boolean): Product? {
        var productIdx = 0
        var product: Product?
        val maxIdx = selectionItems.size -1

        do {
            productIdx = if (productIdx + 1 >= maxIdx) 0 else productIdx + 1
            product = selectionItems[productIdx]
            printProduct(product)
        }
        while ((product == null) || ((product.getStock() == 0) && available))

        printProduct(selectionItems[productIdx])

        var updownControl = true
        var key = NONE

        while (key != KEY_SELECT) {
            key = waitKey(timeout)

            if (key == KEY_SELECT && selectionItems[productIdx].getStock() == 0) {
                clear()
                message("Product ${product?.getName()}", "not available", TIME_LINE)
                break
            }
            else if (key == KEY_SELECT && selectionItems[productIdx].getStock() != 0 || !available){
                return selectionItems[productIdx]
            }

            if (key == KEY_CHANGE) {
                updownControl = !updownControl
                signGraph = if (updownControl) "\u0004" else " "
            }
            else if (updownControl) {
                if (key == KEY_UP) {
                    while (true) {
                        productIdx = if (productIdx - 1 < 0) maxIdx else productIdx - 1
                        product = selectionItems[productIdx]
                        printProduct(product)
                        if (key == KEY_CHANGE) { updownControl = !updownControl }
                        break
                    }
                }
                else if (key == KEY_DOWN) {
                    while (true) {
                        productIdx = if (productIdx + 1 > maxIdx) 0 else productIdx + 1
                        product = selectionItems[productIdx]
                        printProduct(product)
                        if (key == KEY_CHANGE) { updownControl = !updownControl }
                        break
                    }
                }
            }
            else {
                productIdx = if (productIdx == -1) 0 else productIdx * 10 + key.code - 48   // ASCII '0' = 48
                productIdx = if (productIdx < maxIdx) productIdx else key.code - 48
            }
            printProduct(selectionItems[productIdx])
            continue
        }
    return null
    }
}