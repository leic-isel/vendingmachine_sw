/*
*  LIC - ISEL 2021/22
*
*  SerialEmiter - envia para os modulos de hardware LCD e Dispenser os
*                 dados que recebe do controlo de software
*
*  O mapeamento para o Output Port da placa vai ser mapeado de acordo com o seguinte
*  esquema de acordo com os bits disponiveis:
*            7   6   5  4       3   2    1   0
*      In  : 0  BUSY 0  0       0   0    0   0
*      Out : 0   0   0  0       0 SCLK  SDX  0
*
*    @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin


object SerialEmitter {
    private const val BUSY_MASK = 0x40
    private const val SCLK_MASK = 0x04
    private const val SDX_MASK = 0x02

    enum class Destination{DISPENSER, LCD}

    fun init(){}

    fun isBusy(): Boolean = HAL.isBit(BUSY_MASK)

    fun send(address: Destination, data: Int){
        while (isBusy()){continue}
        starCondition()
        prepareData(data.shl(1) + address.ordinal, if (address == Destination.LCD) 6 else 5)
    }

    private fun starCondition(){
        HAL.clrBits(SCLK_MASK)
        HAL.setBits(SDX_MASK)
        HAL.clrBits(SDX_MASK)
    }

    private fun prepareData(data: Int, frameLen: Int){
        var parity = 1
        var dataCopy = data
        var frame = frameLen

        while (frame > 0) {
            val bitToSend = dataCopy.and(1)
            parity = parity.xor(bitToSend)
            sendBit(bitToSend)
            dataCopy = dataCopy.ushr(1)
            frame--
        }
        sendBit(parity)
        HAL.setBits(SDX_MASK)
        HAL.clrBits(SCLK_MASK)
    }

    private fun sendBit(bit: Int){
        if (bit == 1) HAL.setBits(SDX_MASK) else HAL.clrBits(SDX_MASK)
        HAL.clrBits(SCLK_MASK)
        HAL.setBits(SCLK_MASK)
    }
}