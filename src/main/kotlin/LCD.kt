/*
*  LIC - ISEL 2021/22
*
*  LCD - permite enviar os comandos e carateres para escrever
*        no LCD modelo HD44780U, 16x2 com dimensão de 5x8 dots/caracter
*        usando instruções a 4bit
*
*  O LCD deve ser inicializado e durante a inicialização devemos definir
*  o valor de:
*       DL:0 interface 4 bits; DL:1 interface 8 bits
*             use DL:0
*       N:0 display de 1 linha; N:1 display de 2 linhas
*           use N:1
*       F: 0 caracter com 5x8 pontos; F:1 caracter com 5x10 pontos
*            use F:0
*
*
*  Para utilizar o LCD, para apresentar carateres temos que:
*
*  A coordenada de memoria DDRM associada à posição display (AC)
*       00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F
*       40 41 42 43 44 45 46 47 48 49 4A 4B 4C 4D 4E 4F
*  instrução: 1 (AC) (AC) (AC) (AC) (AC) (AC) (AC)
*
*  No display podemos controlar
*       D:0 entire display off; D:1 display in
*       C:0 cursor off; C:1 cursor on
*       B:0 cursor blink off; B:1 cursor blink on
*  instrução: 0 0 0 0 0 1 D C B
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import isel.leic.utils.Time

object LCD {
    private const val CLEAR_DISPLAY_CMD = 0x1
    private const val DISPLAY_INIT_CMD = 0x3
    private const val DISPLAY_CURSOR_ON_CMD = 0xF
    private const val DISPLAY_CURSOR_OFF_CMD = 0xC
    private const val DISPLAY_OFF_CMD = 0x8
    private const val DISPLAY_ON_CMD = 0xF
    private const val DISPLAY_RETURN_HOME = 0x2
    private const val ENTRY_MODE_SET_CMD = 0x6
    private const val NIBBLE_MASK = 0xF
    private const val RS_CMD_VALUE = false
    private const val RS_DATA_VALUE = true
    private const val SET_CGRAM_ADDRESS_CMD = 0x40

    fun init(){
        init4bit()
        defineNewChars()
        posCursor(0, 0)
        writeCMD(DISPLAY_OFF_CMD)
    }

    private fun defineChar(pos: Int, data: IntArray) {
        writeCMD(SET_CGRAM_ADDRESS_CMD.or(8 * pos))
        for (i in 0 .. 7) { writeDATA(data[i]) }
    }

    private fun defineNewChars() {
        val euroSymbol = intArrayOf(7, 8, 30, 8, 30, 8, 7, 0)
        val arrows = intArrayOf(4, 14, 31, 4, 31, 14, 4, 0)
        defineChar(0, euroSymbol)
        defineChar(4, arrows)
    }

    fun posCursor(line: Int, column: Int){
        val pos = (if (line != 0) 64 else 0).or(column.and(0x3F))
        writeCMD(0x80.or(pos))
    }

    private fun init4bit() {
        Time.sleep(15L)
        writeNibble(RS_CMD_VALUE, DISPLAY_INIT_CMD)
        Time.sleep(5L)
        writeNibble(RS_CMD_VALUE, DISPLAY_INIT_CMD)
        Time.sleep(1L)
        writeNibble(RS_CMD_VALUE, DISPLAY_INIT_CMD)
        writeNibble(RS_CMD_VALUE, DISPLAY_RETURN_HOME)
        writeCMD(0x28)  // define N:1 F:0
        writeCMD(DISPLAY_OFF_CMD)
        writeCMD(CLEAR_DISPLAY_CMD)
        writeCMD(ENTRY_MODE_SET_CMD)  // define I/D:1, S:0  (Increment, Cursor move)
        on()
    }

    fun clear() =  writeCMD(CLEAR_DISPLAY_CMD)

    fun on() =  writeCMD(DISPLAY_ON_CMD)

    fun off() = writeCMD(DISPLAY_OFF_CMD)

    fun cursorOFF() = writeCMD(DISPLAY_CURSOR_OFF_CMD)

    fun cursorON() = writeCMD(DISPLAY_CURSOR_ON_CMD)

    fun writeChar(c: Char)  = writeDATA(c.code)

    fun writeString(str: String){
        var b = 0
        val i = str.length
        while (b < i) {
            writeChar(str[b])
            b++
        }
    }

    private fun writeNibble(rs: Boolean, data: Int) = SerialEmitter.send(SerialEmitter.Destination.LCD, data.shl(1).or(if(rs) 1 else 0))

    private fun writeCMD(data: Int) = writeByte(RS_CMD_VALUE, data)

    private fun writeDATA(data: Int) = writeByte(RS_DATA_VALUE, data)

    private fun writeByte(rs: Boolean, data: Int){
        writeNibble(rs, data.shr(4).and(NIBBLE_MASK))
        writeNibble(rs, data.and(NIBBLE_MASK))
    }
}

