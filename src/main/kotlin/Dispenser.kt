/*
*  LIC - ISEL 2021/22
*
*  Dispenser - controla o estado do Dispenser de produtos
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */

package main.kotlin

object Dispenser {
    fun init(){}

    fun dispense(id: Int) = SerialEmitter.send(SerialEmitter.Destination.DISPENSER, id)

    fun isFinished(): Boolean = !SerialEmitter.isBusy()
}