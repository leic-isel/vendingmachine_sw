/*
*  LIC - ISEL 2021/22
*
*  CoinAcceptor - deteta se existe moeda inserida aceitando-a,
*                 pode depois guarda-la ou devolve-la
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import isel.leic.utils.Time

object  CoinAcceptor {
    private const val ACCEPT_MASK = 0x10
    private const val COIN_MASK = 0x20
    private const val COLLECT_MASK = 0x20
    private const val RETURN_MASK = 0x40

    fun init(){
        HAL.setBits(COIN_MASK)
        sleep()
        HAL.clrBits(COIN_MASK)
    }

    fun acceptCoin(){
        if (hasCoin()) {
            HAL.setBits(ACCEPT_MASK)
            while (true) {
                if (!hasCoin()) {
                    HAL.clrBits(ACCEPT_MASK)
                    break
                }
            }
        }
    }

    fun collectCoins(){
        HAL.setBits(COLLECT_MASK)
        sleep()
        HAL.clrBits(COLLECT_MASK)
    }

    fun ejectCoins() {
        HAL.setBits(RETURN_MASK)
        sleep()
        HAL.clrBits(RETURN_MASK)
    }

    fun hasCoin(): Boolean = HAL.isBit(COIN_MASK)

    private fun sleep() = Time.sleep(1000L)
}