/*
*  LIC - ISEL 2021/22
*
*  CoinDeposit - responsavel por manipular (ler, atualizar e guardar)
*                o ficheiro CoinDeposit.txt
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import java.io.FileNotFoundException
import kotlin.system.exitProcess


object CoinDeposit {
    private var deposit = FileAccess
    private  var numberOfCoins: Int = 0

    fun init() = deposit.init("CoinDeposit.txt")

    fun load(): Boolean {
        init()
        return try {
            numberOfCoins = deposit.read()[0].substringBefore(';').toInt()
            true
        }
        catch (error: FileNotFoundException) {
            println("Error opening file CoinDeposit.txt")
            false
        }
        finally { deposit.close() }
    }

    fun save(): Boolean {
        try {
            deposit.clear()
            deposit.write("$numberOfCoins;")
            close()
            return true
        }
        catch (error: FileNotFoundException) {
            println("Error saving file CoinDeposit.txt")
            return false
        }
    }

    fun update(value: Int) { numberOfCoins += value }

    private fun close() {
        deposit.close()
        //if (!save()) exitProcess(-1)
    }
}