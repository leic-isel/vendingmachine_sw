/*
*  LIC - ISEL 2021/22
*
*  Product - objeto criado a partir da leitura do ficheiro PRODUCTS.txt
*            tem como propriedades, ID, name, stock e price
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

class Product(product: List<String>) {
    private var id = product[0].trim().toInt()  // a
    private var name = product[1].trim()  // b
    private var stock = product[2].trim().toInt()  // c
    private var price = product[3].trim().toInt()

    fun getProduct() = this

    fun getID(): Int = id

    fun getName(): String = name

    fun getStock(): Int = stock

    fun getPrice(): Int = price

    fun updateStock(newStock: Int){ this.stock = newStock}

    override fun toString(): String = "$id; $name, $stock, $price"
}