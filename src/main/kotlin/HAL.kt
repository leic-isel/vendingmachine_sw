/*
*   LIC - ISEL 2021/22
*
*   HAL - virtualiza o acesso ao sistema UsbPort
*         lê, escreve e limpa bits
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import isel.leic.UsbPort

// Virtualiza o acesso ao sistema UsbPort
object HAL {
    private var lastOutput = 0x00

    // inicia a class
    fun init() = clrBits(0xFF)

    // retorna true se o bit tiver valor lógico '1'
    fun isBit(mask: Int): Boolean = readBits(mask) != 0

    // retorna os valores dos bits representados por mask presentes no UsbPort
    fun readBits(mask: Int) = UsbPort.`in`().and(mask)

    // escreve nos bits representados por mask o valor lógico '1'
    fun writeBits(mask: Int, value: Int) {
        clrBits(mask)
        setBits(value.and(mask))
    }

    // coloca os bits representados por mask no valor lógico '1'
    fun setBits(mask: Int) {
        lastOutput = lastOutput.or(mask)
        usbOut()
    }

    // coloca os bits representados por mask no valor lógico '0'
    fun clrBits(mask: Int) {
        lastOutput = lastOutput.and(mask.inv())
        usbOut()
    }

    // executa UsbPort.out
    private fun usbOut() = UsbPort.out(lastOutput)
}