/*
*  LIC - ISEL 2021/22
*
*  M - interface entre o hardware que ativa o modo de manutenção e
*      o software que permite a mudança de estado da aplicação
*
*  O bit a ser lido deve ser o bit maior peso do Input
*           7 6 5 4  3 2 1 0
*  Input:   M 0 0 0  0 0 0 0
*
*  @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

object M {
    private const val M_MASK = 0x80

    fun init(){}

    fun isMaintenanceMode(): Boolean = HAL.isBit(M_MASK)
}