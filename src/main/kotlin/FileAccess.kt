/*
*  LIC - ISEL 2021/22
*
*  FileAccess - objecto auxilar para facilitar a leitura e escrita de ficheiros txt
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
 */
package main.kotlin

import java.io.File
import java.io.FileOutputStream
import java.io.Writer

object FileAccess {
    private lateinit var fileOutputStream: FileOutputStream
    private lateinit var file: File

    fun init(fileName: String) {
        fileOutputStream = FileOutputStream(fileName, true)
        file = File(fileName)
    }

    fun read(): List<String> = file.bufferedReader().readLines()

    fun write(text: String) = file.writeText(text)
    
    fun clear() = file.writeText("")

    fun close() {
        fileOutputStream.flush()
        fileOutputStream.close()
    }
}

