/*
*   LIC - ISEL 2021/22
*
*   Program starter
*
*   @author Grupo 6, Nuno Venancio e Miguel Queluz
*
*/
package main.kotlin

import kotlin.system.exitProcess

fun main(args: Array<String>) {
    App.init()
    App.run()
    App.close()
    exitProcess(0)
}
